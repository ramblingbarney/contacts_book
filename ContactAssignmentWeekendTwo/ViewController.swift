//
//  ViewController.swift
//  ContactsAssignment
//
//  Created by The App Experts on 19/10/2019.
//  Copyright © 2019 Conor O'Dwyer. All rights reserved.
//

import UIKit

var textInputName: UITextField!
var textInputNumber: UITextField!
var button: UIButton!
var model: ContactsModel!
var contactsTable: UITableView!

class ViewController: UIViewController {
    
    let cellId = "someRandomLettersNumberes294848dkskdsjls"

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.white
        model = ContactsModel()
        setupUI()
        setUpConstraints()
        contactsTable.dataSource = self
        contactsTable.delegate = self
        contactsTable.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        contactsTable.register(SubtitleTableViewCell.self, forCellReuseIdentifier: cellId)
    }


    private func setupUI() {

        textInputName = UITextField(frame: CGRect(x: 20, y: 100, width: 300, height: 40))
        textInputName.placeholder = "Contact Name"
        textInputName.font = UIFont.systemFont(ofSize: 15)
        textInputName.borderStyle = UITextField.BorderStyle.roundedRect
        view.addSubview(textInputName)

        textInputNumber = UITextField(frame: CGRect(x: 20, y: 100, width: 300, height: 40))
        textInputNumber.placeholder = "(XXX) 000 000 000"
        textInputNumber.font = UIFont.systemFont(ofSize: 15)
        textInputNumber.borderStyle = UITextField.BorderStyle.roundedRect
        view.addSubview(textInputNumber)

        button = UIButton(frame: CGRect(x:10 , y: 190, width: 100,height: 100))
        button.setTitle("Create Contact", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = .clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.black.cgColor
        button.tag = 0
        button.addTarget(self, action: #selector(didClickButton), for: .touchUpInside)
        view.addSubview(button)
        
        contactsTable = UITableView(frame: .zero, style: .plain)
        view.addSubview(contactsTable)

    }

     func setUpConstraints() {

         let margins = view.layoutMarginsGuide

        textInputName.translatesAutoresizingMaskIntoConstraints = false
        textInputName.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 20.0).isActive = true
        textInputName.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -20.0).isActive = true
        textInputName.topAnchor.constraint(equalTo: margins.topAnchor, constant: 30.0).isActive = true
        view.addSubview(textInputName)

        textInputNumber.translatesAutoresizingMaskIntoConstraints = false
        textInputNumber.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 20.0).isActive = true
        textInputNumber.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -20.0).isActive = true
        textInputNumber.topAnchor.constraint(equalTo: textInputName.bottomAnchor, constant: 20.0).isActive = true
        view.addSubview(textInputNumber)

        button.translatesAutoresizingMaskIntoConstraints = false
        button.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 20.0).isActive = true
        button.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -20.0).isActive = true
        button.topAnchor.constraint(equalTo: textInputNumber.bottomAnchor, constant: 20.0).isActive = true
        view.addSubview(button)
         
        contactsTable.translatesAutoresizingMaskIntoConstraints = false
        contactsTable.topAnchor.constraint(equalTo:button.bottomAnchor, constant: 20.0).isActive = true
        contactsTable.leadingAnchor.constraint(equalTo:view.leadingAnchor, constant: 10.0).isActive = true
        contactsTable.trailingAnchor.constraint(equalTo:view.trailingAnchor, constant: -20.0).isActive = true
        contactsTable.bottomAnchor.constraint(equalTo:view.bottomAnchor, constant: -20.0).isActive = true
        view.addSubview(contactsTable)

     }

    @objc func didClickButton(_ sender: UIButton!) -> Void {
        
        switch sender.tag {
        case 0:
            guard let contactName = textInputName.text else { return }
            guard let contactNumber = textInputNumber.text else { return }
            
            if !contactName.isEmpty {
                model.saveContact(name: contactName, number: contactNumber)
                textInputName.text = ""
                textInputNumber.text = ""
                contactsTable.reloadData()
            } else {
                let alertController = UIAlertController(title: "Invalid Contact", message: "Name Required", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default)
                alertController.addAction(OKAction)
                present(alertController, animated: true, completion: nil)
            }
        default:
            print("no action")
        }
        
     }
}

class ContactsModel {
    
    var contacts: [[String]]
    var numberOfSections: Int {
        if numberOfRows > 0 {
            return 1
        } else {
            return 0
        }
    }
    var numberOfRows: Int { return contacts.count }
    var section: String { return "Contacts" }
    
    init() {
        contacts = []
    }
    
    func saveContact(name: String, number: String) -> Void {
    
        if contacts.count > 0 {
            for (index, element) in contacts.enumerated() {
                if element[0] == name {
                    contacts[index][1] = number
                    return
                }
            }
            contacts.append([name, number])
        } else {
            contacts.append([name, number])
        }
        

        let temp = contacts
        contacts = []

        contacts = temp.sorted { $0[0] < $1[0] }

    }
    
    func contacts(atIndexPath indexPath: IndexPath) -> [String]? {
        
        let rows = model.numberOfRows
        
        guard indexPath.row >= 0 && indexPath.row < rows else {
            return nil
        }
        return contacts[indexPath.row]
    }

}

class SubtitleTableViewCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension ViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return model.section
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return model.numberOfSections
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.numberOfRows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)

        guard let contact = model.contacts(atIndexPath: indexPath) else {
            return cell
        }

        cell.textLabel?.text = contact[0]
        cell.detailTextLabel?.text = contact[1]
        
        if contact[1].isEmpty {
            cell.imageView?.image = nil
        } else {
            cell.imageView?.image = UIImage(named: "phoneIcon")
        }

    
        return cell
    }
    
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !model.contacts[indexPath.row][1].isEmpty {
            
            if let phoneCallURL = URL(string: "telprompt://\(model.contacts[indexPath.row][1])") {

                 let application:UIApplication = UIApplication.shared
                 if (application.canOpenURL(phoneCallURL)) {
                     if #available(iOS 10.0, *) {
                         application.open(phoneCallURL, options: [:], completionHandler: nil)
                     } else {
                         // Fallback on earlier versions
                          application.openURL(phoneCallURL as URL)

                     }
                 }
             }
        }

    }

}



